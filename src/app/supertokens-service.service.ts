import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import SuperTokens from 'supertokens-website';
import axios from "axios";
SuperTokens.addAxiosInterceptors(axios);

@Injectable({
  providedIn: 'root'
})
export class SupertokensServiceService {

  constructor(private http: HttpClient) {}

  sendData(email){
    let url = 'http://localhost:3000/auth';
    return this.http.post(url,email);
  }

  login(email){
    // let headers = new HttpHeaders()
    //   .set('rid', 'passwordless');
    let object;
    let url = 'http://localhost:3000/auth/signinup/code';
    // return this.http.post(url,email, {headers});
    let headers = {'rid': 'passwordless'};
    return axios.post(url, email)
    .then(function (data) {
      console.log(data.data);
      object = data.data;
    })
    .catch(function (error) {
      console.log(error);
      object = error;
    });
  }

  consume(data){
    let headers = new HttpHeaders()
      .set('rid', 'passwordless');
    let url = 'http://localhost:3000/auth/signinup/code/consume';
    return this.http.post(url,data, {headers});
  }

  logout(){
    let headers = new HttpHeaders()
      .set('rid', 'session');
    let url = 'http://localhost:3000/auth/signout';
    return this.http.post(url,{}, {headers});
  }

  // verifySession(){
  //   let headers = new HttpHeaders()
  //     .set('rid', 'passwordless');
  //   let url = 'http://localhost:3000/sessioninfo';
  //   return this.http.get(url);
  // }

}
