import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./supertokens/supertokens.module').then(mod => mod.SupertokensModule),
    data: { preload: true }  // Custom property we will use to track what route to be preloaded
  },
  {
    path: 'magic',
    loadChildren: () => import('./magiclink/magiclink.module').then(mod => mod.MagiclinkModule),
    data: { preload: true }  // Custom property we will use to track what route to be preloaded
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'auth',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
