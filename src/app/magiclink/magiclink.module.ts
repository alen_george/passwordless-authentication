import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MagiclinkRoutingModule } from './magiclink-routing.module';
import { MagiclinkComponent } from './magiclink.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MagiclinkComponent
  ],
  imports: [
    CommonModule,
    MagiclinkRoutingModule,
    FormsModule
  ]
})
export class MagiclinkModule { }
