import { Component, OnInit } from '@angular/core';
import { Magic } from 'magic-sdk';

@Component({
  selector: 'app-magiclink',
  templateUrl: './magiclink.component.html',
  styleUrls: ['./magiclink.component.scss']
})
export class MagiclinkComponent implements OnInit {

  ngOnInit(): void {
  }

  magic = new Magic('pk_live_6609C20212AE6DE7');
  mssg = '';
  section = 'login';
  userMetadata: any;
  email:string="";
  show = false;
  constructor() {
    this.authenticateUsingEmail();
  }

  async authenticateUsingEmail() {
    if (window.location.pathname === 'http://localhost:4200/magic') {
      try {
        /* Complete the "authentication callback" */
        await this.magic.auth.loginWithCredential();

        /* Get user metadata including email */
        this.userMetadata = await this.magic.user.getMetadata();
        this.section = 'loggedIn';
        this.show = false;
      } catch {
        /* In the event of an error, we'll go back to the login page */
        window.location.href = "http://localhost:4200/magic";
      }
    } else {
      const isLoggedIn = await this.magic.user.isLoggedIn();

      /* Show login form if user is not logged in */
      this.section = 'login';

      if (isLoggedIn) {
        /* Get user metadata including email */
        this.userMetadata = await this.magic.user.getMetadata();
        this.section = 'loggedIn';
        console.log(this.userMetadata);
        this.show = false;
      }
    }
  }

  /* Implement Login Handler */
  async handleLogin(event) {
    event.preventDefault();

    if(this.email){
      this.mssg='Email has been send to your mail. <br> Kindly click the button in mail to login uing passwordless authentication.';
    }
    else{
      this.mssg="Email field is empty."
    }
    this.show = true;
    let email = this.email;
    let redirectURI = "http://localhost:4200/magic";
    if (email) {
      await this.magic.auth.loginWithMagicLink({
        email,
        showUI: true,
        redirectURI
      });
      this.authenticateUsingEmail();
    }
  }

  /* Implement Logout Handler */
  async handleLogout() {
    await this.magic.user.logout();
    this.authenticateUsingEmail();
    this.show = false;
  }

}
