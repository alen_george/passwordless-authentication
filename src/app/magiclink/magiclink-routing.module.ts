import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MagiclinkComponent } from './magiclink.component';

const routes: Routes = [
  { path: '', component: MagiclinkComponent },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagiclinkRoutingModule { }
