import { Component, OnInit } from '@angular/core';
import SuperTokens from 'supertokens-website';
import { SupertokensServiceService } from '../supertokens-service.service';
import axios from 'axios';
SuperTokens.addAxiosInterceptors(axios);
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-supertokens',
  templateUrl: './supertokens.component.html',
  styleUrls: ['./supertokens.component.scss'],
})
export class SupertokensComponent implements OnInit {
  mssg = '';
  section = 'login';
  userMetadata: any;
  email: string = '';
  show = false;
  otp;
  parsedValue: any;
  userId;
  linkId = "";
  preAuthSessionId = "";
  refresh = true;
  constructor(
    private service: SupertokensServiceService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.preAuthSessionId = params.preAuthSessionId;
    });
  }

  ngOnInit(): void {
    SuperTokens.init({
      apiDomain: 'http://localhost:3000',
      apiBasePath: '/auth',
    });
    this.verifySession();

    setInterval(this.verifySession, 3000);

  }

  async handleLogin() {
    let email = {
      email: this.email,
    };
    let url = 'http://localhost:3000/auth/signinup/code';
    await axios
      .post(url, email)
      .then((data) => {
        let value = JSON.stringify(data.data);
        this.parsedValue = JSON.parse(value);
        this.section = 'consume';
        console.log(this.parsedValue);
      })
      .catch(function (error) {
        console.log('Error is' + error.message);
      });
  }

  async otpAuthenticate() {
    let consumedValue = {
      preAuthSessionId: this.parsedValue.preAuthSessionId,
      deviceId: this.parsedValue.deviceId,
      userInputCode: this.otp,
    };
    let url = 'http://localhost:3000/auth/signinup/code/consume';
    await axios
      .post(url, consumedValue)
      .then((data) => {
        console.log(data);
        let value = JSON.stringify(data.data);
        this.userMetadata = JSON.parse(value);
        this.userId = this.userMetadata.user.id;
        console.log(this.userMetadata);
        this.section = 'loggedIn';
      })
      .catch(function (error) {
        console.log('Error is' + error.message);
      });
  }

  async magicLinkAuthenticate() {
    console.log(this.linkId);
    console.log(this.preAuthSessionId);
    let consumedValue = {
      preAuthSessionId: this.preAuthSessionId,
      linkCode: this.linkId,
    };
    let url = 'http://localhost:3000/auth/signinup/code/consume';
    await axios
      .post(url, consumedValue)
      .then((data) => {
        console.log(data.data);
        let value = JSON.stringify(data.data);
        this.userMetadata = JSON.parse(value);
        this.userId = this.userMetadata.user.id;
        this.section = 'loggedIn';
        window.location.href = "http://localhost:4200/auth";
      })
      .catch(function (error) {
        console.log('Error is' + error.message);
      });
  }

  handleLogout() {
    SuperTokens.signOut();
    this.section = 'login';
  }

  async verifySession() {
    // user has already logged in
    if (await SuperTokens.doesSessionExist()) {
      this.userId = await SuperTokens.getUserId();
      this.section = 'loggedIn';
    } else {
      // user has not logged in yet
      this.section = 'login';
      if (this.preAuthSessionId) {
        this.linkId = this.router.url.substring(
          this.router.url.indexOf('#') + 1
        );
        this.linkId = this.linkId.slice(0, -1);
        this.magicLinkAuthenticate();
      }
    }
  }
}
