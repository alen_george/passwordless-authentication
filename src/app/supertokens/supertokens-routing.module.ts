import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupertokensComponent } from './supertokens.component';

const routes: Routes = [
  { path: '', component: SupertokensComponent },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupertokensRoutingModule { }
