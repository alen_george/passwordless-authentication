import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SupertokensRoutingModule } from './supertokens-routing.module';
import { SupertokensComponent } from './supertokens.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SupertokensComponent
  ],
  imports: [
    CommonModule,
    SupertokensRoutingModule,
    FormsModule
  ]
})
export class SupertokensModule { }
