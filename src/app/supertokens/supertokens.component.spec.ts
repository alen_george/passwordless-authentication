import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupertokensComponent } from './supertokens.component';

describe('SupertokensComponent', () => {
  let component: SupertokensComponent;
  let fixture: ComponentFixture<SupertokensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupertokensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupertokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
