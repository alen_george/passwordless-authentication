import { TestBed } from '@angular/core/testing';

import { SupertokensServiceService } from './supertokens-service.service';

describe('SupertokensServiceService', () => {
  let service: SupertokensServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SupertokensServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
